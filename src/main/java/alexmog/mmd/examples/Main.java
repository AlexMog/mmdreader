package alexmog.mmd.examples;
import java.io.FileNotFoundException;
import java.io.IOException;
import alexmog.mmd.reader.MMDReader;
import alexmog.mmd.reader.models.Collider;
import alexmog.mmd.reader.models.MapData;
import alexmog.mmd.reader.models.MapObject;

public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        MapData data = MMDReader.readMapFile("map.mmd");
        System.out.println("Objects:" + data.getMapObjects().size());
        for (MapObject obj : data.getMapObjects()) {
            System.out.println(obj);
            for (Collider c : obj.getColliders()) System.out.println(c.getData());
            System.out.println("===");
        }
    }

}
