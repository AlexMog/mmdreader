package alexmog.mmd.reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.io.LittleEndianDataInputStream;

public class Utils {
	public static String readUTF8(LittleEndianDataInputStream stream) throws IOException {
		List<Byte> buffer = new ArrayList<>();
		
		byte b;
        while ((b = stream.readByte()) != 0) buffer.add(b);

        byte[] primitive = new byte[buffer.size()];
        
        int i = 0;
        for (Byte byt : buffer) primitive[i++] = byt;
        
        return new String(primitive, "UTF8");
    }
}
