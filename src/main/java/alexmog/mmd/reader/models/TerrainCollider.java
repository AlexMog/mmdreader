package alexmog.mmd.reader.models;

import java.io.IOException;

import com.google.common.io.LittleEndianDataInputStream;

public class TerrainCollider extends ColliderData
{
    private int mHeightmapResolution;
    private int mDetailResolution;
    private Vector3 mSize;
    private float[][] mHeights;

    public TerrainCollider(Bounds bounds, int heightmapResolution, int detailResolution, Vector3 size, float[][] heights)
    {
    	super(bounds);
        mSize = size;
        mHeightmapResolution = heightmapResolution;
        mDetailResolution = detailResolution;
        mHeights = heights;
    }
    
    public int getHeightmapResolution() {
    	return mHeightmapResolution;
    }
    
    public int getDetailResolution() {
    	return mDetailResolution;
    }
    
    public Vector3 getSize() {
    	return mSize;
    }
    
    public float[][] getHeights() {
    	return mHeights;
    }

    public static TerrainCollider deserialize(LittleEndianDataInputStream stream) throws IOException
    {
    	Bounds bounds = Bounds.deserialize(stream);

        int heightmapResolution = stream.readInt();
        int detailResolution = stream.readInt();

        Vector3 size = new Vector3(stream.readFloat(), stream.readFloat(), stream.readFloat());

        float[][] heights = new float[heightmapResolution][heightmapResolution];
        for (int y = 0; y < heightmapResolution; y++)
            for (int x = 0; x < heightmapResolution; x++)
                heights[y][x] = stream.readFloat();

        return new TerrainCollider(bounds, heightmapResolution, detailResolution, size, heights);
    }
    
    @Override
    public String toString() {
        return String.format("TerrainCollider(%s|heightmapResolution:%d|detailResolution:%d|size:%d)", getBounds(), mHeightmapResolution, mDetailResolution, mSize);
    }
}
