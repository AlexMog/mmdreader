package alexmog.mmd.reader.models;

import java.util.ArrayList;
import java.util.List;

public class MapData {
	private List<MapObject> mMapObjects = new ArrayList<MapObject>();

    public List<MapObject> getMapObjects() {
    	return mMapObjects;
    }
}
