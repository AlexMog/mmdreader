package alexmog.mmd.reader.models;

import java.io.IOException;

import com.google.common.io.LittleEndianDataInputStream;

public class Bounds {
	private Vector3 mCenter;
    private Vector3 mSize;

    public Bounds(Vector3 center, Vector3 size)
    {
        mCenter = center;
        mSize = size;
    }
    
    public Vector3 getCenter() {
    	return mCenter;
    }
    
    public Vector3 getSize() {
    	return mSize;
    }

    public static Bounds deserialize(LittleEndianDataInputStream stream) throws IOException
    {
        return new Bounds(new Vector3(stream.readFloat(), stream.readFloat(), stream.readFloat()), new Vector3(stream.readFloat(), stream.readFloat(), stream.readFloat()));
    }
    
    @Override
    public String toString() {
        return String.format("Bounds(center:%s|size:%s)", mCenter, mSize);
    }
}
