package alexmog.mmd.reader.models;

import java.io.IOException;

import com.google.common.io.LittleEndianDataInputStream;

public class BoxCollider extends ColliderData
{
    public BoxCollider(Bounds bounds)
    {
    	super(bounds);
    }

    public static BoxCollider deserialize(LittleEndianDataInputStream stream) throws IOException
    {
        return new BoxCollider(Bounds.deserialize(stream));
    }
    
    @Override
    public String toString() {
        return String.format("BoxCollider(%s)", getBounds());
    }
}
