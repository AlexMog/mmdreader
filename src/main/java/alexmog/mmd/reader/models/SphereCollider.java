package alexmog.mmd.reader.models;

import java.io.IOException;

import com.google.common.io.LittleEndianDataInputStream;

public class SphereCollider extends ColliderData
{
    private float mRadius;

    public SphereCollider(Bounds bounds, float radius)
    {
    	super(bounds);
        mRadius = radius;
    }
    
    public float getRadius() {
    	return mRadius;
    }

    public static SphereCollider deserialize(LittleEndianDataInputStream stream) throws IOException
    {
        return new SphereCollider(Bounds.deserialize(stream), stream.readFloat());
    }
    
    @Override
    public String toString() {
        return String.format("SphereCollider(%s|radius:%f)", getBounds(), mRadius);
    }
}
