package alexmog.mmd.reader.models;

public abstract class ColliderData {
	private Bounds mBounds;

    public ColliderData(Bounds bounds)
    {
        mBounds = bounds;
    }
    
    public Bounds getBounds() {
    	return mBounds;
    }
}
