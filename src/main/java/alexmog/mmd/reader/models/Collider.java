package alexmog.mmd.reader.models;

import java.io.IOException;

import com.google.common.io.LittleEndianDataInputStream;

public class Collider {
	private ColliderType mType;
    private ColliderData mData;

    public Collider(ColliderType type, ColliderData data)
    {
        mType = type;
        mData = data;
    }
    
    public ColliderType getColliderType() {
    	return mType;
    }
    
    public ColliderData getData() {
    	return mData;
    }

    public static Collider deserialize(LittleEndianDataInputStream stream) throws IOException
    {
        ColliderData data = null;

        ColliderType type = ColliderType.values()[stream.readInt()];
        if (type == ColliderType.Box) data = BoxCollider.deserialize(stream);
        else if (type == ColliderType.Capsule) data = CapsuleCollider.deserialize(stream);
        else if (type == ColliderType.Sphere) data = SphereCollider.deserialize(stream);
        else if (type == ColliderType.Terrain) data = TerrainCollider.deserialize(stream);
        else if (type == ColliderType.Mesh) data = MeshCollider.deserialize(stream);

        return new Collider(type, data);
    }

    public enum ColliderType
    {
        Box,
        Capsule,
        Mesh,
        Sphere,
        Terrain
    }
}
