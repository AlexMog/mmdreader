package alexmog.mmd.reader.models;

import java.io.IOException;

import com.google.common.io.LittleEndianDataInputStream;

public class MeshCollider extends ColliderData
{
    private boolean mConvex;
    private Vector3[] mVertices;
    private int[] mTriangles;

    public MeshCollider(Bounds bounds, boolean convex, Vector3[] vertices, int[] triangles)
    {
    	super(bounds);
        mConvex = convex;
        mVertices = vertices;
        mTriangles = triangles;
    }
    
    public boolean isConvex() {
    	return mConvex;
    }
    
    public Vector3[] getVertices() {
    	return mVertices;
    }
    
    public int[] getTriangles() {
    	return mTriangles;
    }

    public static MeshCollider deserialize(LittleEndianDataInputStream stream) throws IOException
    {
        Bounds bounds = Bounds.deserialize(stream);

        boolean convex = stream.readBoolean();

        int verticesCount = stream.readInt();
        Vector3[] vertices = new Vector3[verticesCount];
        for (int i = 0; i < verticesCount; ++i) vertices[i] = new Vector3(stream.readFloat(), stream.readFloat(), stream.readFloat());

        int trianglesCount = stream.readInt();
        int[] triangles = new int[trianglesCount];
        for (int i = 0; i < trianglesCount; ++i) triangles[i] = stream.readInt();

        return new MeshCollider(bounds, convex, vertices, triangles);
    }
    
    @Override
    public String toString() {
        return String.format("MeshCollider(%s|convex:%b|vertices:%d|triangles:%d)", getBounds(), mConvex, mVertices.length, mTriangles.length);
    }
}
