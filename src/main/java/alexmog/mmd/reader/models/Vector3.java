package alexmog.mmd.reader.models;

public class Vector3 {
	private float mX, mY, mZ;
	
	public Vector3(float x, float y, float z) {
		mX = x;
		mY = y;
		mZ = z;
	}
	
	public float getX() {
		return mX;
	}
	
	public float getY() {
		return mY;
	}
	
	public float getZ() {
		return mZ;
	}
	
	@Override
	public String toString() {
	    return String.format("Vector3(%f,%f,%f)", mX, mY, mZ);
	}
}
