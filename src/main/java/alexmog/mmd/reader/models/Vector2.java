package alexmog.mmd.reader.models;

public class Vector2 {
	private float mX, mY;
	
	public Vector2(float x, float y) {
		mX = x;
		mY = y;
	}
	
	public float getX() {
		return mX;
	}
	
	public float getY() {
		return mY;
	}
	
	@Override
	public String toString() {
	    return String.format("Vector2(%f,%f)", mX, mY);
	}
}
