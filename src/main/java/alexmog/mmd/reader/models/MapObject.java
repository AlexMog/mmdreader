package alexmog.mmd.reader.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.io.LittleEndianDataInputStream;

import alexmog.mmd.reader.Utils;

public class MapObject {
	private String mName;
	private Vector3 mPosition;
	private Vector3 mRotation;
	private boolean mStatic;
	private List<Collider> mColliders;

    public MapObject(String name, Vector3 position, Vector3 rotation, boolean isStatic)
    {
        mName = name;
        mPosition = position;
        mRotation = rotation;
        mStatic = isStatic;
        mColliders = new ArrayList<Collider>();
    }
    
    public List<Collider> getColliders() {
    	return mColliders;
    }
    
    public String getName() {
    	return mName;
    }
    
    public Vector3 getPostion() {
    	return mPosition;
    }
    
    public Vector3 getRotation() {
    	return mRotation;
    }
    
    public boolean isStatic() {
    	return mStatic;
    }

    public static MapObject deserialize(LittleEndianDataInputStream stream) throws IOException
    {
        String name = Utils.readUTF8(stream);
        Vector3 position = new Vector3(stream.readFloat(), stream.readFloat(), stream.readFloat());
        Vector3 rotation = new Vector3(stream.readFloat(), stream.readFloat(), stream.readFloat());
        boolean isStatic = stream.readBoolean();

        MapObject ret = new MapObject(name, position, rotation, isStatic);

        int collidersCount = stream.readInt();
        for (int i = 0; i < collidersCount; ++i) ret.getColliders().add(Collider.deserialize(stream));

        return ret;
    }
    
    @Override
    public String toString() {
        return String.format("MapObject(name:%s|pos:%s|rot:%s|static:%b|colliders:%d)", mName, mPosition, mRotation, mStatic, mColliders.size());
    }
}
