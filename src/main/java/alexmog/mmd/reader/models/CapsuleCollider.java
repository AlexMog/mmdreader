package alexmog.mmd.reader.models;

import java.io.IOException;

import com.google.common.io.LittleEndianDataInputStream;

public class CapsuleCollider extends ColliderData
{
    private float mRadius;
    private float mHeight;
    private AxisType mAxis;

    public CapsuleCollider(Bounds bounds, float radius, float height, AxisType axis)
    {
    	super(bounds);
        mRadius = radius;
        mHeight = height;
        mAxis = axis;
    }
    
    public float getRadius() {
    	return mRadius;
    }
    
    public float getHeight() {
    	return mHeight;
    }
    
    public AxisType getAxis() {
    	return mAxis;
    }

    public static CapsuleCollider deserialize(LittleEndianDataInputStream stream) throws IOException
    {
        return new CapsuleCollider(Bounds.deserialize(stream), stream.readFloat(), stream.readFloat(), AxisType.values()[stream.readInt()]);
    }
    
    @Override
    public String toString() {
        return String.format("CapsuleCollider(%s|radius:%f|height:%f|axis:%s)", getBounds(), mRadius, mHeight, mAxis);
    }

    public enum AxisType
    {
        X,
        Y,
        Z
    }
}
