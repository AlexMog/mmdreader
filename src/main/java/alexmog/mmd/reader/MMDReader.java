package alexmog.mmd.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import com.google.common.io.LittleEndianDataInputStream;

import alexmog.mmd.reader.models.MapData;
import alexmog.mmd.reader.models.MapObject;

public class MMDReader {
	public static MapData readMapFile(String mapFilePath) throws FileNotFoundException, IOException {
		MapData data = new MapData();
		try (LittleEndianDataInputStream stream = new LittleEndianDataInputStream(new GZIPInputStream(new FileInputStream(new File(mapFilePath))))) {
			int objectsCount = stream.readInt();
			for (int i = 0; i < objectsCount; ++i) data.getMapObjects().add(MapObject.deserialize(stream));
		}
		return data;
	}
}
