# MMDReader  
MMD is a format used to export Unity "worlds" as simple collision datas.  
It is used by my personnal projects.  

## Install  
To install the lib, you will need Java8 and Maven.  
Then go to the git directory that you have cloned, and simply type
```bash
mvn install
```  
Then, you can add it to your own maven dependencies
```
<dependency>
    <groupId>alexmog</groupId>
    <artifactId>MMDReader</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Usage
```java
import java.io.FileNotFoundException;
import java.io.IOException;

import alexmog.mmd.reader.MMDReader;
import alexmog.mmd.reader.models.MapData;
import alexmog.mmd.reader.models.MapObject;

public class Test {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		MapData data = MMDReader.readMapFile("myMap.mmd");
		
		for (MapObject object : data.getMapObjects()) {
			System.out.println(object.getName() + " @(" + object.getPostion().getX() + ", " + object.getPostion().getY() +", " + object.getPostion().getZ() + ")");
		}
	}

}
```